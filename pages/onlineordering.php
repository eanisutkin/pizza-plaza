<?php
    include "components/Article.php";
    $article = new Article();
    $allPizzaz = $article->getAllPizzaz();
    $pizzasWithExtras = $article->getPizzasWithExtras();
?>
<link rel="stylesheet" href="assets/css/onlineordering.css">

<ul class="pizzaList">
<?php
    $orderControlId = 0;
    foreach ($allPizzaz as $pizza) {
        $orderControlId++;
        echo '<li class="listItem">'.
             '<div class="container">'.
                '<div class="row rowItem">'.
                    '<div class="col-sm-12 col-md-6">'.
                        '<h1>'.$pizza["name"].': '.$pizza["price"].'€</h1>'.
                        '<p>'.$pizza["description"].'</p>'.
                    '</div>'.
                    '<div class="col-sm-12 col-md-6">'.
                        '<div class="container">'.
                            '<div class="row orderControl">'.
                                '<button id="substrQuantity" class="btn" >-</button>'.
                                '<input id="quantityVal" type="text" class="col-sm-2" value="1">'.
                                '<button id="addQuantity" class="btn">+</button>'.
                                '<div class="col-sm-3">'.
                                    '<button id="addBtn" class="btn btn-primary w-100">Add</button>'.
                                '</div>'.
                             '</div>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="container">'.
                    '<div class="row">';
        foreach ($pizzasWithExtras as $extras) {
            if($extras["id"] === $pizza["id"]) {
                echo '<div class="col-sm-auto">'.
                        '<input class="extras" type="checkbox" name='.$extras["name"].'> '.
                        '<label class="extras">'.$extras["name"].': '.$extras["price"].'€</label>'.
                    '</div>';
            }
        }

        echo    '</div>'.
            '</div>'.
            '</li>';
    }
?>
</ul>

<script src="assets/js/shoppingCart.js"></script>
<script src="assets/js/onlineordering.js"></script>
