$(document).ready(function(){
    $('.orderControl').each(function(){
        $(this).find('#quantityVal').keydown(function(e){
            e.preventDefault();
        });

        $(this).find('#substrQuantity').click(function(){
            var val = parseInt($(this).next().val(), 10);
            if( val == 1){
                $(this).next().val(99);
            } else {
                $(this).next().val(val - 1);
            }
        });

        $(this).find('#addQuantity').click(function(){
            var val = parseInt($(this).prev().val(), 10);
            if(val == 99){
                $(this).prev().val(1);
            } else {
                $(this).prev().val(val + 1);
            }
        });

        $(this).find("#addBtn").click(function(){
            var info = $(this).parents('.rowItem').find('h1').html().split(": ");

            var val = parseInt($(this).parents('.orderControl').find('#quantityVal').val());
            var name = info[0];
            var price = parseFloat(info[1].slice(0, -1), 10);
            shoppingCart.addItemToCart(name, price, val);
        });
    });
});
