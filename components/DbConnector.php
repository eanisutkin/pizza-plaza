<?php

class DbConnector
{
    private  $server = "";
    private  $dbname = "";
    private  $username = "";
    private  $password = "";

    /**
     * DbConnector constructor.
     * @param string $host
     * @param string $username
     * @param string $password
     * @param string $dbname
     */
    public function __construct() {
        $this->server = "localhost";
        $this->dbname = "pizza-plaza";
        $this->username = "site-admin";
        $this->password = "!@mySuperSecretAgetn007InATardisPassword";
    }

    function createConn() {
        return new mysqli($this->server, $this->username, $this->password, $this->dbname);
    }
}
