<?php
include "DbConnector.php";

class Article
{
    private $dbConnector;

    function __construct()
    {
        $this->dbConnector=new DbConnector();
    }

    function getAllPizzaz() {
        $conn = $this->dbConnector->createConn();
        if($conn->connect_error) {
            die("Connection to database failed with following error: ". $conn->connect_error);
        }

        $sql = "SELECT * FROM Pizzas";

        $res = $conn->query($sql);

        $allPizzaz = array();

        if($res->num_rows > 0) {
            while($row = $res->fetch_assoc()) {
                array_push($allPizzaz, $row);
            }
        }

        return $allPizzaz;
    }

    // This assumes that extras are really ingridients and not really extras.
    function getPizzasWithExtras() {

        // Create connection
        $conn =$this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = "SELECT Pizzas.name, Pizzas.id, Extras.name, Extras.price
                FROM Pizzas
                INNER JOIN Pizza_has_Extra
                On Pizzas.id = Pizza_has_Extra.Pizzas_ID
                INNER JOIN Extras
                on Pizza_has_Extra.Extras_ID = Extras.ID
                WHERE Extras.isChoosable = 1";

        $res = $conn->query($sql);

        $pizzasWithextras = array();

        if($res->num_rows > 0) {
            while($row = $res->fetch_assoc()) {
                array_push($pizzasWithextras, $row);
            }
        }

        return $pizzasWithextras;
    }

}
