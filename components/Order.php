<?php

include "DbConnector.php";

class Order {
    private $id;
    private $customerID;
    private $dbConnector;

    function __construct() {
        $this->dbConnector = new DbConnector();
    }

    function getOrderId() {
        return $this->id;
    }

    function getCustomerId() {
        return $this->CustomerID;
    }

    function setCustomerID($id) {
        $this->customerID = $id;
    }

    /*
    *  This needs do get customer data from somewhere. Still have no idea where.
    *  If a user enters online ordering page and presses add, the only way to identify him is session cookie.
    *  But session cookie does not provide any required data. So we are kind of stuck in a loop.
    *  We can store and distinguish orders through session storage though, but again only based on sesion cookie.
    *  Our form for entering user data is on checkout page, but by then we already have an order, creating a user is late
    *  as we can't insert into table orders an id that does not exist in customers, we and we dont have that id till we get customer data.
    *  The problem is that we get this data after we have to create an order.
    *
    *  Potential solutions 1: Do not actually store orders in Database until we can create new customer. Use session storage.
    *  Debatable as task explicitly states to use Order table, which we don't really.
    *  It also gives an ability to "remember" customer by session cookie and autofill fields.
    *  However we must create a 1 to 1 mapping between session cookie and user id, to avoid creating duplicate entries for 1 cookie.
    *  This also has anoter downside - if and when the sesion cookie changes, the user will loose his order,
    *  and we would have no way of pulling it from database as we basically lost the id.
    *
    *  Potential solution 2: Create a customer with empty data. We only need the id. Use this id to create Order.
    *  Debatable, and possibly very bad, since we can have many empty customers in database this way.
    *  Actually we will have an empty customer each time a user opens the page since we don't know users id.
    *  We can reduce number of empty users by creating a 1 to 1 mapping between session cookie and user id, but we will still get them.
    *
    *  On one hand our database and server will go nuts if we do SQL change for every operation.
    *  The first option reduses server load to roughly O(1) queries per user instead of O(n) queries per user, which is much faster.
    *
    *  On the other hand we cab get required working though database using second solution increasing server load.
    *  It is possible to reduce number of queries to roughly O(1), but we will also need a 1 to 1 mapping for that.
    *
    *  Side note: if we take into the account a task about showing all orders to admins,
    *  we have to make sure we only show orders for users who actually entered required used data, second way first wa seems easier.
    *
    *  Side note 2: Authentication is not usable at all since we have no unique values in customer table.
    *  We have to make some field unique to make sure no duplicates are cteated.
    */
    function createOrder($customer) {
        $this->createCustomer($sutomer)
    }

    function createCustomer($customer) {
        $firstname="";
        $lastname="";
        $street="";
        $streetnumber=0;
        $zip="";
        $city="";
        $phone=0;

        // Create connection
        $conn = $this->dbConnector->createConn();

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $sql = $conn->prepare("INSERT INTO `Customer`( `firstname`, `lastname`, `street`, `streetnumber`, `zip`, `city`, `phone`) ".
               "VALUES(?, ?, ?, ?, ?, ?, ?)");

        $sql->bind_param("sssissi", $firstname, $lastname, $street, $streetnumber, $zip, $city, $phone);
        if($sql->execute()) {
            $this->id = $sql->insert_id;
        }
    }
}
